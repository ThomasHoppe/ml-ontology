# Machine Learning (ML) Ontology

The goal of the project is to develop a (quite) general machine learning ontology. 
In the onlology the various concepts, methods, application scenarios and algorithms etc. are related to each other.

In a next step additional special ontologies beside the ml onlology are planned for different applications.
An examples of such an application is teaching. If a student, researcher or software developer 
want to learn a special topic of ML a learning path with links to appropriate teaching material can be generated from the 
ontologies. 
 
